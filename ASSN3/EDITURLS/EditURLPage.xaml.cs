﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class EditUrlPage : ContentPage
	{
		private SQLiteAsyncConnection _connection;
		public ObservableCollection<WebUrl> _url;
		private int SelectedNumber;

		public EditUrlPage(WebUrl selectedClass, int selection)
		{
			InitializeComponent();

			_connection = DependencyService.Get<ISQLiteDb>().GetConnection();

			BindingContext = selectedClass;
			SelectedNumber = selection;
		}

		async void EditUrl(object sender, EventArgs e)
		{
			var url = await _connection.Table<WebUrl>().ToListAsync();
			_url = new ObservableCollection<WebUrl>(url);

			await DisplayAlert("Thank You", "Url Successfully Updated", "OK");

			var urlBlock = url[SelectedNumber];
			urlBlock.Title = TitleNew.Text;
			urlBlock.Url = UrlNew.Text;
			urlBlock.Images = ImageNew.Text;

			await _connection.UpdateAsync(urlBlock);
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}