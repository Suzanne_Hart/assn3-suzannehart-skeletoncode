﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class AddUrlPage : ContentPage
	{
		private SQLiteAsyncConnection _connection;

		public AddUrlPage()
		{
			InitializeComponent();

			_connection = DependencyService.Get<ISQLiteDb>().GetConnection();
		}

		async void AddUrl(object sender, EventArgs e)
		{
			var url = new WebUrl { Title = TitleNew.Text, Images = ImageNew.Text, Url = UrlNew.Text };
			await _connection.InsertAsync(url);

			await DisplayAlert("Thank You", "Url Successfully Added", "OK");

			TitleNew.Text = "";
			ImageNew.Text = "";
			UrlNew.Text = "";
		}

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}
